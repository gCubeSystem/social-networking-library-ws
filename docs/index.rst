.. Social Service Client documentation master file, created by
   sphinx-quickstart on Tue Aug  2 16:11:12 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Social Service's documentation!
=================================================

The Social Service is a RESTful application which exposes operations coming from both the gCube `Social-Networking-Library <https://wiki.gcube-system.org/gcube/Social_Networking_Library>`_ 
and the `User Management <https://wiki.gcube-system.org/gcube/UserManagement_Core>`_  components. 
The former is the bridge between the Cassandra Cluster (on which social data are stored) and the social applications, 
whereas the latter is the abstraction layer over the enabling technology for the Identity and Access Management service, in terms of: roles, users and 
organizations and how they are mapped to D4Science concepts.

The web services exposes a subset of such functionalities over **https in a standard, reliable and secure way**.

See the vailable REST-API at a glance on `its API docs <../api-docs/index.html>`_.





Base URL
==================

In the production environment, its current value is https://api.d4science.org/rest

Authorization (how to contact the service)
==================

D4Science adopts state-of-the-art industry standards for authentication and authorization. 
Specifically, the implementation fully adopts `OIDC (OpenID Connect) <https://openid.net/connect>`_ for authentication and UMA 2 (User Managed Authorization) for authorization flows. 
`JSON Web Token (JWT) Access token <https://jwt.io/>`_ are used for both authentication and authorization.

Obtain your Bearer token here: https://dev.d4science.org/how-to-access-resources

Java Client
==================

The methods of the Web Service can be called by writing your own REST client application or by using already existing REST client plugins. We provide the following Java Client out-of-the-box.


 .. TIP::
    If you're coding in Java it is recommended that you use this Java Client.

**Maven Coordinates**

.. code:: xml

 <groupId>org.gcube.social-networking</groupId>
 <artifactId>social-service-client</artifactId>
 <version>[1.1.0, 2.0.0)</version>
 
**Methods Result**

The service exposes `its methods <../api-docs/index.html>`_ using a  standard naming approach. Moreover, they accept (in case of http POST/PUT methods) JSON objects. 

 .. IMPORTANT::
   The result of all methods is always a JSON object as per below:

.. code:: javascript

 {
 "success": false/true,
 "message": ...,
 "result": ...,
 }

Where
 - success reports if the request succeeded or failed;
 - message is a status/error message that can be checked in case of errors (success equals false);
 -result is the current result object (it can be a list, a single object and so on depending on the invoked method).

*Inputs are automatically validated before the request is served.*

      
**Usage examples**

- Send a Message
      
.. code:: java

 import org.gcube.social_networking.social_networking_client_library.MessageClient;
 import org.gcube.social_networking.socialnetworking.model.beans.Message;
 import org.gcube.social_networking.socialnetworking.model.beans.Recipient;

 //send
 MessageClient messagesClient = new MessageClient();
 List<Recipient> rec = Arrays.asList(new Recipient("andrea.rossi"));
 Message message = new Message(
                "Test message", 
                "Sending message via client " + System.currentTimeMillis(), 
                new ArrayList<Recipient>(rec));
 String idMessage = messagesClient.writeMessage(message);
 
- Get User Profile

.. code:: java

 import org.gcube.social_networking.social_networking_client_library.PeopleClient;

	PeopleClient getProfile = new PeopleClient();
	UserProfile profile = getProfile.getProfile();
	logger.debug("Profile retrieved is " + profile);

- Get Posts in a VRE

.. code:: java

 import org.gcube.social_networking.social_networking_client_library.PostClient;

	PostClient postClient = new PostClient();
	List<Feed> vrePosts = postClient.getPostsVRE();
	logger.debug("VRE posts are " + vrePosts);