This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "social-networking-library-ws"


## [v3.1.0]

- gcube-smartgears-bom to 2.5.1


## [v3.0.1] - 2023-04-22

- Feature 27286: removed noisy logs
- Bug 27218: advance social networking library version


## [v3.0.0] - 2023-12-06

- Feature #26193, new impl for get users by role in a VRE
- Replace Astyanx Java client for Cassandra 2 with Datastax client for Cassandra 4
- Add REST resources for all the functions in social networking library


## [v2.9.1] - 2023-09-28

- Fix for Bug #25760 not closing connection to distributed cached
- Removed memcached dep


## [v2.9.0] - 2023-02-13

 - Feature #24456 social-networking rest api endpoints (comments, Likes, GetPost)


## [v2.8.0] - 2022-10-20

- Feature #23891 Refactored following updates social lib
- Feature #23847 Social service: temporarily block of notifications for given username(s)
- Feature #23439 Please allow an IAM client to send notifications OPEN
- Feature #23991 Support attachments through notification / message API
- Feature #23995 added support for set Message read / unread
- Feature #24022 added get posts By PostId with range filter parameters and get Comments By PostId


## [v2.7.0] - 2022-09-12

 - Sphinx documentation added


## [v2.6.2] - 2022-07-27

- Bug fix #23695 social service 2/people/profile api fails sometimes with error 500
- get-email method was not working with UMA tokens


## [v2.6.0] - 2022-05-16

- Feature #23186, Full notifications support for social service


## [v2.5.0] - 2022-04-06

- enunciate integration


## [v2.4.0] - 2021-04-30

- Feature #21179 porting to storagehub messages


## [v2.3.3] - 2020-10-08

- removed jackson dependency


## [v2.3.0] - 2019-10-02

- added support for mentions with @ in user's post as well as application posts
- Added the property cataloguePortletURL and the method [#19440]


## [v2.2.4] - 2019-07-22

- Bug fix #17269, SocialNetworking service passes wrong email sender in email notifications


## [v2.2.3] - 2019-04-05

- added get-oauth-profile method for returning Google similar JSON Profile
- added support for caches (ehcache)
- Fix for #10997
- Support for tickets #9333, #9332, #9276


## [v2.0.0] - 2017-02-01

- Moved as service on top of SmartGears
- Exploiting Liferay's Web APIs


## [v1.4.0] - 2016-12-01

- Custom jackson object mapper is used to have snake case fields'names
- Improved users version 2 resource with more methods


## [v1.3.0] - 2016-10-10

- Moved to new Authorization Framework
- Added method to send Messages
- Added new version for the APIs that accept/return JSON
- gCube Token is discovered before every call is executed by a filter


## [v1.0.0] - 2015-12-21

- First release

