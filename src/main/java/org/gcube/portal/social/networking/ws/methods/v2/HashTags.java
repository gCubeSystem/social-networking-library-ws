package org.gcube.portal.social.networking.ws.methods.v2;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portal.databook.server.DatabookStore;
import org.gcube.portal.social.networking.liferay.ws.GroupManagerWSBuilder;
import org.gcube.portal.social.networking.ws.outputs.ResponseBean;
import org.gcube.portal.social.networking.ws.utils.CassandraConnection;
import org.gcube.portal.social.networking.ws.utils.ErrorMessages;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * REST interface for the social networking library (hash tags).
 * 
 * @author Ahmed Ibrahim ISTI-CNR
 */
@Path("2/hashtags")
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
		@RequestHeader(name = "Content-Type", description = "application/json")
})
public class HashTags {

	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(HashTags.class);

	@GET
	@Path("get-hashtags-and-occurrences/")
	@Produces({ MediaType.APPLICATION_JSON })
	@StatusCodes({
			@ResponseCode(code = 200, condition = "Hashtags and occurrences retrieved, reported in the 'result' field of the returned object"),
			@ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	/**
	 * @return hashtags in the context bound to the auth token
	 */
	public Response getHashTagsAndOccurrences() {

		Caller caller = AuthorizationProvider.instance.get();
		String username = caller.getClient().getId();
		String context = ScopeProvider.instance.get();
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;

		logger.debug("User " + username + " has requested hashtags of context " + context);

		try {
			DatabookStore datastore = CassandraConnection.getInstance().getDatabookStore();
			// TODO handle the case of VO and ROOT
			boolean isVRE = GroupManagerWSBuilder.getInstance().getGroupManager().isVRE(
					GroupManagerWSBuilder.getInstance().getGroupManager().getGroupIdFromInfrastructureScope(context));
			if (isVRE) {
				Map<String, Integer> map = datastore.getVREHashtagsWithOccurrence(context);
				responseBean.setResult(map);
				responseBean.setSuccess(true);
			} else {
				responseBean.setMessage("Please provide a VRE token. VO and ROOT VO cases are not yet managed.");
				responseBean.setResult(false);
			}
		} catch (Exception e) {
			logger.error("Failed to retrieve hashtags", e);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}
}
