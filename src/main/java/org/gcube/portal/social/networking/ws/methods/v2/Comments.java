package org.gcube.portal.social.networking.ws.methods.v2;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portal.databook.shared.Comment;
import org.gcube.portal.databook.shared.ex.FeedIDNotFoundException;
import org.gcube.portal.social.networking.ws.inputs.CommentInputBean;
import org.gcube.portal.social.networking.ws.outputs.ResponseBean;
import org.gcube.portal.social.networking.ws.utils.CassandraConnection;
import org.gcube.portal.social.networking.ws.utils.ErrorMessages;
import org.gcube.portal.social.networking.ws.utils.Filters;
import org.gcube.portal.social.networking.ws.utils.SocialUtils;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * REST interface for the social networking library (comments).
 * 
 * @author Ahmed Ibrahim ISTI-CNR
 */
@Path("2/comments")
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
		@RequestHeader(name = "Content-Type", description = "application/json")
})
public class Comments {

	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Comments.class);

	/*
	 * Retrieve the list of comments belonging to the post id (key) of the token in
	 * the related context
	 * 
	 * @param key the key as in the POST JSON representation
	 * 
	 * @pathExample
	 * /get-comments-by-post-id?key=9ea137e9-6606-45ff-a1a2-94d4e8760583
	 * 
	 * @return the list of comments belonging to the post identified by the key in
	 * the context identified by the token
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-comments-by-post-id")
	@StatusCodes({
			@ResponseCode(code = 200, condition = "The list of comments is put into the 'result' field"),
			@ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response getAllCommentsByPostId(
			@NotNull @QueryParam("key") String key) {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Caller caller = AuthorizationProvider.instance.get();
		String context = ScopeProvider.instance.get();
		String username = caller.getClient().getId();
		List<Comment> comments = null;

		try {
			logger.debug("Retrieving comments for user id " + username);
			comments = CassandraConnection.getInstance().getDatabookStore().getAllCommentByPost(key);
			Filters.filterCommentsPerContext(comments, context);
			responseBean.setResult(comments);
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}

	/*
	 * Retrieve the list of comments belonging to the owner of the token in the
	 * related context
	 * 
	 * @return the list of comments belonging to the owner of the token in the
	 * related context.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-comments-user")
	@StatusCodes({
			@ResponseCode(code = 200, condition = "The list of comments is put into the 'result' field"),
			@ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response getCommentsUser() {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Caller caller = AuthorizationProvider.instance.get();
		String context = ScopeProvider.instance.get();
		String username = caller.getClient().getId();
		List<Comment> comments = null;

		try {
			logger.debug("Retrieving comments for user id " + username);
			comments = CassandraConnection.getInstance().getDatabookStore().getRecentCommentsByUserAndDate(username, 0);
			Filters.filterCommentsPerContext(comments, context);
			responseBean.setResult(comments);
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}

	/*
	 * Retrieve comments of the token owner in the context bound to the token itself
	 * and filter them by date
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-comments-user-by-time")
	public Response getCommentsUserByTime(
			@QueryParam("time") @Min(value = 0, message = "time cannot be negative") long timeInMillis)
			throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Caller caller = AuthorizationProvider.instance.get();
		String context = ScopeProvider.instance.get();
		String username = caller.getClient().getId();
		List<Comment> comments = null;

		try {
			logger.debug("Retrieving comments for user id " + username);
			comments = CassandraConnection.getInstance().getDatabookStore().getRecentCommentsByUserAndDate(username,
					timeInMillis);
			Filters.filterCommentsPerContext(comments, context);
			responseBean.setResult(comments);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}

	/**
	 * Create a new comment to a post having as owner the auth token's owner
	 * 
	 * @param comment The CommentInputBean object
	 * @return
	 * @throws ValidationException
	 */
	@POST
	@Path("comment-post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@StatusCodes({
			@ResponseCode(code = 201, condition = "Successfull created, the new comment is reported in the 'result' field of the returned object"),
			@ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response writeComment(
			@NotNull(message = "Comment to write is missing") @Valid CommentInputBean comment)
			throws ValidationException {
		Caller caller = AuthorizationProvider.instance.get();
		String username = caller.getClient().getId();
		logger.debug("Request of writing a comment coming from user " + username);
		String context = ScopeProvider.instance.get();
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;

		try {
			String postId = comment.getPostid();
			String commentText = comment.getText();
			String userid = username;
			Date time = new Date();

			String postOwnerId = CassandraConnection.getInstance().getDatabookStore().readPost(postId).getEntityId();
			Comment theComment = SocialUtils.commentPost(userid, time, postId, commentText, postOwnerId, context);
			if (theComment != null)
				logger.debug("Added comment " + theComment.toString());
			else {
				logger.error("Unable to write comment");
				responseBean.setMessage("Unable to write comment, something went wrong please see server log");
				responseBean.setSuccess(false);
				status = Status.INTERNAL_SERVER_ERROR;
				return Response.status(status).entity(responseBean).build();
			}

			responseBean.setResult(theComment);
			responseBean.setSuccess(true);
			return Response.status(status).entity(responseBean).build();
		} catch (FeedIDNotFoundException ex) {
			logger.error("Unable to find a post comment", ex);
			responseBean.setMessage("Could not reach the DB to write the comment, something went wrong");
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).entity(responseBean).build();
		} catch (Exception e) {
			logger.error("Unable to write comment", e);
			responseBean.setMessage("Could not reach the DB to write the comment, something went wrong");
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).entity(responseBean).build();
		}
	}
}
