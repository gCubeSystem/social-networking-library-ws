package org.gcube.portal.social.networking.ws.methods.v2;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.portal.databook.shared.Attachment;
import org.gcube.portal.databook.shared.Comment;
import org.gcube.portal.databook.shared.Invite;
import org.gcube.portal.databook.shared.InviteOperationResult;
import org.gcube.portal.databook.shared.InviteStatus;
import org.gcube.portal.databook.shared.Like;
import org.gcube.portal.databook.shared.Notification;
import org.gcube.portal.databook.shared.NotificationChannelType;
import org.gcube.portal.databook.shared.NotificationType;
import org.gcube.portal.databook.shared.Post;
import org.gcube.portal.databook.shared.RangePosts;
import org.gcube.portal.social.networking.ws.outputs.ResponseBean;
import org.gcube.portal.social.networking.ws.utils.CassandraConnection;
import org.gcube.portal.social.networking.ws.utils.ErrorMessages;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * REST interface for the social networking library.
 * 
 * @author Ahmed Ibrahim ISTI-CNR
 */
@Ignore
@Path("2/lib")
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
		@RequestHeader(name = "Content-Type", description = "application/json")
})
public class Lib {

	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Lib.class);

	// Comments
	//
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("add-comment-lib/")
	public Response addCommentLib(@NotNull(message = "Comment to write is missing") @Valid Comment comment)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Writing comment with commentid " + comment.getKey());
			boolean result = CassandraConnection.getInstance().getDatabookStore().addComment(comment);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("read-comment-by-id-lib/")
	public Response readCommentByIdLib(@QueryParam("commentid") String commentid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Getting comment with commentid " + commentid);
			Comment result = CassandraConnection.getInstance().getDatabookStore().readCommentById(commentid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to find comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-comments-by-post-id-lib/")
	@StatusCodes({
			@ResponseCode(code = 200, condition = "The list of comments is put into the 'result' field"),
			@ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response getAllCommentsByPostIdLib(
			@NotNull @QueryParam("postid") String postid) {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<Comment> comments = null;

		try {
			logger.debug("Retrieving comments for  postid " + postid);
			comments = CassandraConnection.getInstance().getDatabookStore().getAllCommentByPost(postid);
			responseBean.setResult(comments);
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-comments-by-user-and-date-lib/")
	public Response getRecentCommentsByUserAndDateLib(
			@QueryParam("time") @Min(value = 0, message = "time cannot be negative") long timeInMillis,
			@QueryParam("username") String username) throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<Comment> comments = null;

		try {
			logger.debug("Retrieving comments for user id " + username);
			comments = CassandraConnection.getInstance().getDatabookStore().getRecentCommentsByUserAndDate(username,
					timeInMillis);
			responseBean.setResult(comments);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("edit-comment-lib/")
	public Response editCommentLib(
			@NotNull(message = "Comment to edit is missing") @Valid Comment comment) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Writing comment with commentid " + comment.getKey());
			boolean result = CassandraConnection.getInstance().getDatabookStore().editComment(comment);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete-comment-lib/")
	public Response deleteCommentLib(
			@QueryParam("commentid") String commentid,
			@QueryParam("postid") String postid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("deleting comment with commentid " + commentid);
			boolean result = CassandraConnection.getInstance().getDatabookStore().deleteComment(commentid, postid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to delete comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	// Hashtags
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-hashtag-lib/")
	public Response saveHashTagsLib(
			@QueryParam("feedid") String feedid,
			@QueryParam("vreid") String vreid,
			@NotNull(message = "hashtag to save is missing") @Valid List<String> hashtags) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Saving hashtags ");
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveHashTags(feedid, vreid, hashtags);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to save hashtags.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete-hashtag-lib/")
	public Response deleteHashTagsLib(
			@QueryParam("feedid") String feedid,
			@QueryParam("vreid") String vreid,
			@NotNull(message = "hashtag to delete is missing") @Valid List<String> hashtags)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("deleting hashtags ");
			boolean result = CassandraConnection.getInstance().getDatabookStore().deleteHashTags(feedid, vreid,
					hashtags);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to delete hashtags.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-hashtag-comment-lib/")
	public Response saveHashTagsCommentLib(
			@QueryParam("commentid") String commentid,
			@QueryParam("vreid") String vreid,
			@NotNull(message = "hashtag to save is missing") @Valid List<String> hashtags) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Saving hashtags comments ");
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveHashTagsComment(commentid, vreid,
					hashtags);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to save hashtags comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete-hashtag-comment-lib/")
	public Response deleteHashTagsCommentLib(
			@QueryParam("commentid") String commentid,
			@QueryParam("vreid") String vreid,
			@NotNull(message = "hashtag to delete is missing") @Valid List<String> hashtags)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("deleting hashtags ");
			boolean result = CassandraConnection.getInstance().getDatabookStore().deleteHashTagsComment(commentid,
					vreid, hashtags);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to delete hashtags.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-vre-hashtags-occurrences-lib/")
	public Response getVREHashtagsWithOccurrenceLib(
			@QueryParam("vreid") String vreid) throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Map<String, Integer> hashtags = null;

		try {
			logger.debug("getting vre hashtags occurrences " + vreid);
			hashtags = CassandraConnection.getInstance().getDatabookStore().getVREHashtagsWithOccurrence(vreid);
			responseBean.setResult(hashtags);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-vre-hashtags-occurrences-time-lib/")
	public Response getVREHashtagsWithOccurrenceFilteredByTimeLib(
			@QueryParam("vreid") String vreid,
			@QueryParam("time") @Min(value = 0, message = "time cannot be negative") long timestamp)
			throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Map<String, Integer> hashtags = null;

		try {
			logger.debug("getting vre hashtags occurrences " + vreid);
			hashtags = CassandraConnection.getInstance().getDatabookStore()
					.getVREHashtagsWithOccurrenceFilteredByTime(vreid, timestamp);
			responseBean.setResult(hashtags);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-vre-post-by-hashtag-lib/")
	public Response getVREPostsByHashtagLib(
			@QueryParam("vreid") String vreid,
			@QueryParam("hashtag") String hashtag) throws ValidationException {
		hashtag = "#" + hashtag;

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<Post> posts = null;

		try {
			logger.debug("getting vre hashtags occurrences " + vreid);
			posts = CassandraConnection.getInstance().getDatabookStore().getVREPostsByHashtag(vreid, hashtag);
			responseBean.setResult(posts);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such comments.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	// Invites
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("is-existing-invite-lib/")
	public Response isExistingInviteLib(
			@QueryParam("vreid") String vreid,
			@QueryParam("email") String email) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		String inv = null;

		try {
			logger.debug("Retrieving invite for vre " + vreid);
			inv = CassandraConnection.getInstance().getDatabookStore().isExistingInvite(vreid, email);
			responseBean.setResult(inv);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-invite-lib/")
	public Response saveInviteLib(
			@NotNull(message = "invite to save is missing") @Valid Invite invite) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		InviteOperationResult inv = null;

		try {
			inv = CassandraConnection.getInstance().getDatabookStore().saveInvite(invite);
			responseBean.setResult(inv);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("read-invite-lib/")
	public Response readInviteLib(
			@QueryParam("inviteid") String inviteid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Invite inv = null;

		try {
			logger.debug("Retrieving invite for id " + inviteid);
			inv = CassandraConnection.getInstance().getDatabookStore().readInvite(inviteid);
			responseBean.setResult(inv);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("set-invite-status-lib/")
	public Response setInviteStatusLib(
			@QueryParam("vreid") String vreid,
			@QueryParam("email") String email,
			@NotNull(message = "invitestatus to save is missing") @Valid InviteStatus stat) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Boolean inv = null;

		try {
			inv = CassandraConnection.getInstance().getDatabookStore().setInviteStatus(vreid, email, stat);
			responseBean.setResult(inv);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-invited-email-by-vre-lib/")
	public Response getInvitedEmailsByVRELib(
			@QueryParam("vreid") String vreid,
			@NotNull(message = "invitestatus to save is missing") @Valid InviteStatus... stat)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<Invite> inv = null;

		try {
			inv = CassandraConnection.getInstance().getDatabookStore().getInvitedEmailsByVRE(vreid, stat);
			responseBean.setResult(inv);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	// Likes
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("like-lib/")
	public Response likeLib(
			@NotNull(message = "like to add is missing") @Valid Like like) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Writing like with likeid " + like.getKey());
			boolean result = CassandraConnection.getInstance().getDatabookStore().like(like);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("unlike-lib/")
	public Response unlikeLib(
			@QueryParam("userid") String userid,
			@QueryParam("likeid") String likeid,
			@QueryParam("feedid") String feedid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			logger.debug("Writing unlike with likeid " + likeid);
			boolean result = CassandraConnection.getInstance().getDatabookStore().unlike(userid, likeid, feedid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-liked-posts-by-user-lib/")
	public Response getAllLikedPostIdsByUserLib(
			@QueryParam("userid") String userid) throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<String> likes = null;

		try {
			logger.debug("Retrieving likes for user id " + userid);
			likes = CassandraConnection.getInstance().getDatabookStore().getAllLikedPostIdsByUser(userid);
			responseBean.setResult(likes);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-likes-by-post-lib/")
	public Response getAllLikesByPostLib(
			@QueryParam("postid") String postid) throws ValidationException {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		List<Like> likes = null;

		try {
			logger.debug("Retrieving likes for post id " + postid);
			likes = CassandraConnection.getInstance().getDatabookStore().getAllLikesByPost(postid);
			responseBean.setResult(likes);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	// Notifications
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-notification-lib/")
	public Response saveNotificationLib(
			@NotNull(message = "notification to add is missing") @Valid Notification not) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveNotification(not);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("read-notification-lib/")
	public Response readNotificationLib(
			@QueryParam("notid") String notid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			Notification result = CassandraConnection.getInstance().getDatabookStore().readNotification(notid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("set-notification-read-lib/")
	public Response setNotificationReadLib(
			@QueryParam("notid") String notid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().setNotificationRead(notid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-notifications-user/")
	public Response getAllNotificationByUserLib(
			@QueryParam("userid") String userid,
			@QueryParam("limit") int limit) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Notification> result = CassandraConnection.getInstance().getDatabookStore()
					.getAllNotificationByUser(userid, limit);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-unread-notifications-user/")
	public Response getUnreadNotificationsByUserLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Notification> result = CassandraConnection.getInstance().getDatabookStore()
					.getUnreadNotificationsByUser(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-range-notifications-user/")
	public Response getRangeNotificationsByUserLib(
			@QueryParam("userid") String userid,
			@QueryParam("from") int from,
			@QueryParam("quantity") int quantity) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Notification> result = CassandraConnection.getInstance().getDatabookStore()
					.getRangeNotificationsByUser(userid, from, quantity);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("set-all-notification-read-lib/")
	public Response setAllNotificationReadByUserLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().setAllNotificationReadByUser(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("check-unread-notification-lib/")
	public Response checkUnreadNotificationsLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().checkUnreadNotifications(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("check-unread-messages-notification-lib/")
	public Response checkUnreadMessagesNotificationsLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore()
					.checkUnreadMessagesNotifications(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-user-notification-channels-lib/")
	public Response getUserNotificationChannelsLib(
			@QueryParam("userid") String userid,
			@QueryParam("type") String type) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<NotificationChannelType> result = CassandraConnection.getInstance().getDatabookStore()
					.getUserNotificationChannels(userid, NotificationType.valueOf(type));
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("set-notification-preference-lib/")
	public Response setUserNotificationPreferencesLib(
			@QueryParam("userid") String userid,
			@NotNull(message = "notification prefs to add is missing") @Valid Map<NotificationType, NotificationChannelType[]> enabledChannels)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		for (NotificationType notificationType : enabledChannels.keySet()) {
			logger.debug("Type: " + notificationType.toString());
			for (NotificationChannelType channelType : enabledChannels.get(notificationType)) {
				logger.debug(channelType.toString());
			}
		}
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().setUserNotificationPreferences(userid,
					enabledChannels);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write settings.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-notification-preference-lib/")
	public Response getUserNotificationPreferencesLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			Map<NotificationType, NotificationChannelType[]> result = CassandraConnection.getInstance()
					.getDatabookStore().getUserNotificationPreferences(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	// Posts
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-user-post-lib/")
	public Response saveUserPostLib(
			@NotNull(message = "post to add is missing") @Valid Post post) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveUserPost(post);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-attachment-lib/")
	public Response saveAttachmentEntryLib(
			@QueryParam("postid") String postkey,
			@NotNull(message = "attachment to add is missing") @Valid Attachment attachment)
			throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveAttachmentEntry(postkey,
					attachment);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write attachment.", e);
			logger.error(e.getMessage());
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-app-post-lib/")
	public Response saveAppPostLib(
			@NotNull(message = "post to add is missing") @Valid Post post) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().saveAppPost(post);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-post-to-vretimeline-lib/")
	public Response savePostToVRETimelineLib(
			@QueryParam("postid") String postid,
			@QueryParam("vreid") String vreid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().savePostToVRETimeline(postid, vreid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("read-post-lib/")
	public Response readPostLib(
			@QueryParam("postid") String postid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			Post result = CassandraConnection.getInstance().getDatabookStore().readPost(postid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-posts-by-user-date-lib/")
	public Response getRecentPostsByUserAndDateLib(
			@QueryParam("userid") String userid,
			@QueryParam("time") long timeinmillis) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getRecentPostsByUserAndDate(userid,
					timeinmillis);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete-post-lib/")
	public Response deletePostLib(
			@QueryParam("postid") String postid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			boolean result = CassandraConnection.getInstance().getDatabookStore().deletePost(postid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(result);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-posts-by-user-lib/")
	public Response getAllPostsByUserLib(
			@QueryParam("userid") String userid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getAllPostsByUser(userid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-posts-by-app-lib/")
	public Response getAllPostsByAppLib(
			@QueryParam("appid") String appid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getAllPostsByApp(appid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-commented-posts-by-user-lib/")
	public Response getRecentCommentedPostsByUserAndDateLib(
			@QueryParam("userid") String userid,
			@QueryParam("time") long timeinmillis) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore()
					.getRecentCommentedPostsByUserAndDate(userid, timeinmillis);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-portal-privacy-level-posts-lib/")
	public Response getAllPortalPrivacyLevelPostsLib() throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getAllPortalPrivacyLevelPosts();
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-posts-by-user-lib/")
	public Response getRecentPostsByUserLib(
			@QueryParam("userid") String userid,
			@QueryParam("quantity") int quantity) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getRecentPostsByUser(userid,
					quantity);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-posts-by-vre-lib/")
	public Response getAllPostsByVRELib(
			@QueryParam("vreid") String vreid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getAllPostsByVRE(vreid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-posts-by-vre-lib/")
	public Response getRecentPostsByVRELib(
			@QueryParam("vreid") String vreid,
			@QueryParam("quantity") int quantity) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Post> result = CassandraConnection.getInstance().getDatabookStore().getRecentPostsByVRE(vreid,
					quantity);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-recent-posts-by-vre-range-lib/")
	public Response getRecentPostsByVREAndRangeLib(
			@QueryParam("vreid") String vreid,
			@QueryParam("from") int from,
			@QueryParam("quantity") int quantity) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			RangePosts result = CassandraConnection.getInstance().getDatabookStore().getRecentPostsByVREAndRange(vreid,
					from, quantity);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-attachment-by-postid-lib/")
	public Response getAttachmentsByFeedIdLib(
			@QueryParam("postid") String postid) throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<Attachment> result = CassandraConnection.getInstance().getDatabookStore()
					.getAttachmentsByFeedId(postid);
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-all-vre-ids-lib/")
	public Response getAllVREIdsLib() throws ValidationException {
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		try {
			List<String> result = CassandraConnection.getInstance().getDatabookStore().getAllVREIds();
			responseBean.setResult(result);
			responseBean.setMessage("");
			responseBean.setSuccess(true);
		} catch (Exception e) {
			logger.error("Unable to write comment.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return Response.status(status).entity(responseBean).build();
	}

}
