package org.gcube.portal.social.networking.ws.utils;

import org.gcube.common.authorization.library.ClientType;
import org.gcube.common.authorization.library.utils.Caller;
import org.slf4j.LoggerFactory;

/**
 * Tokens utils methods
 */
public class TokensUtils {

	// a user context token (not qualified) has as qualifier the word "TOKEN"
	private static final String DEFAULT_QUALIFIER_USER_TOKEN = "TOKEN";
	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TokensUtils.class);

	/**
	 * Check if it is a service token
	 * 
	 * @return a boolean value
	 */
	public static boolean isServiceToken(Caller caller) {

		return caller.getClient().getType().equals(ClientType.SERVICE);

	}

	/**
	 * Check if it is an application token
	 * 
	 * @return a boolean value
	 */
	public static boolean isApplicationToken(Caller caller) {
		String username = caller.getClient().getId();
		if (username.startsWith("service-account-")) {
			return true;
		}
		return caller.getClient().getType().equals(ClientType.EXTERNALSERVICE);

	}

	/**
	 * Check if it is a container token
	 * 
	 * @return a boolean value
	 */
	public static boolean isContainerToken(Caller caller) {

		return caller.getClient().getType().equals(ClientType.CONTAINER);

	}

	/**
	 * Check if it is a user token
	 * 
	 * @return a boolean value
	 */
	public static boolean isUserToken(Caller caller) {
		logger.debug("\n ****** \n isUserToken: caller.getClient().getType().equals(ClientType.USER) => "
				+ caller.getClient().getType().equals(ClientType.USER));
		String username = caller.getClient().getId();
		if (username.startsWith("service-account-")) {
			return false;
		}
		return caller.getClient().getType().equals(ClientType.USER);

	}

	/**
	 * Check if it is a user token (not qualified)
	 * 
	 * @return a boolean value
	 */
	public static boolean isUserTokenDefault(Caller caller) {
		return isUserToken(caller);
	}

	/**
	 * Check if it is a user token (qualified)
	 * 
	 * @return a boolean value
	 */
	public static boolean isUserTokenQualified(Caller caller) {

		return caller.getClient().getType().equals(ClientType.USER)
				&& !caller.getTokenQualifier().equals(DEFAULT_QUALIFIER_USER_TOKEN);

	}

}
