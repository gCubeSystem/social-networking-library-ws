package org.gcube.portal.social.networking.ws.mappers;

import org.gcube.applicationsupportlayer.social.shared.SocialFileItem;
import org.gcube.applicationsupportlayer.social.shared.SocialSharedFolder;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.FileItemBean;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.FolderBean;

public class WorkspaceItemMapper {

	public WorkspaceItemMapper() {
		// TODO Auto-generated constructor stub
	}

	public static SocialFileItem getFileItem(FileItemBean item) {
		String id = null;
		String name = null;
		String title = null;
		String path = null;
		SocialSharedFolder parent = null;

		try {
			id = item.getId();
			name = item.getName();
			title = item.getTitle();
			path = item.getPath();
			parent = getSharedFolder(item.getParent());
		} catch (Exception e) {
			e.printStackTrace();
		}
		SocialFileItem toReturn = new SocialFileItem(id, name, title, path, parent);
		return toReturn;
	}

	public static SocialSharedFolder getSharedFolder(FolderBean item) {

		String id = null;
		String name = null;
		String title = null;
		String displayName = null;
		String path = null;
		String parentId = null;
		boolean vreFolder = false;
		try {
			id = item.getId();
			name = item.getName();
			title = item.getTitle();
			displayName = item.getDisplayName();
			path = item.getPath();
			parentId = item.getParentId();
			vreFolder = item.isVreFolder();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SocialSharedFolder toReturn = new SocialSharedFolder(id, name, title, displayName, path, parentId, vreFolder);
		return toReturn;
	}
}
