package org.gcube.portal.social.networking.ws.mappers;

import org.gcube.portal.databook.shared.JobStatusType;
import org.gcube.portal.databook.shared.RunningJob;
import org.gcube.social_networking.socialnetworking.model.beans.JobNotificationBean;
import org.gcube.social_networking.socialnetworking.model.beans.JobStatusModelType;

public class JobMapper {

	public JobMapper() {
	}

	public static RunningJob getJob(JobNotificationBean item) {
		String jobId = null;
		String jobName = null;
		JobStatusType status = null;
		String message = null;
		String serviceName = null; // i.e., Dataminer, SmartExecutor..

		try {
			jobId = item.getJobId();
			jobName = item.getJobName();
			status = getType(item.getStatus());
			message = item.getStatusMessage();
			serviceName = item.getServiceName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new RunningJob(jobId, jobName, status, message, serviceName);
	}

	public static JobStatusType getType(JobStatusModelType type) {
		JobStatusType toReturn = null;
		switch (type) {
			case CANCELLED:
				return JobStatusType.CANCELLED;
			case DELETED:
				return JobStatusType.DELETED;
			case FAILED:
				return JobStatusType.FAILED;
			case CANCELLING:
				return JobStatusType.CANCELLING;
			case DELETING:
				return JobStatusType.DELETING;
			case EXECUTING:
				return JobStatusType.EXECUTING;
			case NEW:
				return JobStatusType.NEW;
			case SUBMITTED:
				return JobStatusType.SUBMITTED;
			case SUCCEEDED:
				return JobStatusType.SUCCEEDED;
			case TIMED_OUT:
				return JobStatusType.TIMED_OUT;
			case WAITING:
				return JobStatusType.WAITING;
			default:
				break;
		}
		return toReturn;
	}

}
