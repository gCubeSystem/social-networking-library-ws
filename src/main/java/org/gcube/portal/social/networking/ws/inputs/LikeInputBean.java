package org.gcube.portal.social.networking.ws.inputs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Like Bean object
 */
public class LikeInputBean {

	@JsonProperty("likeid")
	@NotNull(message = "likeid cannot be null")
	@Size(message = "likeid cannot be empty", min = 1)
	private String likeid;

	@JsonProperty("postid")
	@NotNull(message = "postid cannot be null")
	@Size(message = "postid cannot be empty", min = 1)
	private String postid;

	public LikeInputBean() {
		super();
	}

	/**
	 * @param likeid
	 * @param postid
	 */
	public LikeInputBean(String likeid, String postid) {
		super();
		this.likeid = likeid;
		this.postid = postid;
	}

	public String getLikeid() {
		return likeid;
	}

	public void setLikeid(String likeid) {
		this.likeid = likeid;
	}

	public String getPostid() {
		return postid;
	}

	public void setPostid(String postid) {
		this.postid = postid;
	}

	@Override
	public String toString() {
		return "LikeInputBean [likeid=" + likeid + ", postid=" + postid + "]";
	}

}
