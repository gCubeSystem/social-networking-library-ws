package org.gcube.portal.social.networking.ws.docs;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("api-docs")
public class DocsGenerator {

    private static Logger logger = LoggerFactory.getLogger(DocsGenerator.class);

    @GET
    @Path("/{any: .*}")
    public InputStream toDoc(@Context HttpServletRequest req) throws WebApplicationException {
        logger.info(DocsGenerator.class.getSimpleName() + " toDoc called");

        String pathInfo = req.getPathInfo();
        logger.debug("pathInfo {}", pathInfo);
        try {

            if (pathInfo.endsWith("/api-docs")) {
                pathInfo += "index.html";
            }

            if (pathInfo.endsWith("/api-docs/")) {
                pathInfo += "index.html";
            }

            logger.info("going to {}", pathInfo);

            String realPath = req.getServletContext().getRealPath(pathInfo);
            return new FileInputStream(new File(realPath));

        } catch (Exception e) {
            e.printStackTrace();
            // MANAGE THE EXCEPTION
        }
        return null;
    }
}