package org.gcube.portal.social.networking.liferay.ws;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.Iterator;
import java.util.List;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeycloakAPICredentials {

	private static final Logger logger = LoggerFactory.getLogger(KeycloakAPICredentials.class);

	// the singleton obj
	private static KeycloakAPICredentials singleton = new KeycloakAPICredentials();

	// properties that it contains
	private String keycloakURL;
	private String realm;
	private String clientid;
	private String password;

	// Service endpoint properties
	private final static String RUNTIME_RESOURCE_NAME = "IAM";
	private final static String CATEGORY = "Service";

	/**
	 * Private constructor
	 */
	private KeycloakAPICredentials() {
		logger.debug("Building KeycloakAPICredentials object");

		lookupPropertiesFromIs();
		logger.debug("KeycloakAPICredentials object built");
	}

	/**
	 * Read the properties from the infrastructure
	 */
	private void lookupPropertiesFromIs() {

		logger.debug("Starting creating KeycloakAPICredentials");

		String oldContext = ScopeProvider.instance.get();
		ApplicationContext ctx = ContextProvider.get(); // get this info from SmartGears
		ScopeProvider.instance.set("/" + ctx.container().configuration().infrastructure());
		logger.debug("Discovering liferay user's credentials in context "
				+ ctx.container().configuration().infrastructure());

		try {
			List<ServiceEndpoint> resources = getConfigurationFromIS();
			if (resources.size() == 0) {
				logger.error("There is no Runtime Resource having name " + RUNTIME_RESOURCE_NAME + " and Category "
						+ CATEGORY + " in this scope.");
				throw new Exception("There is no Runtime Resource having name " + RUNTIME_RESOURCE_NAME
						+ " and Category " + CATEGORY + " in this scope.");
			} else {
				for (ServiceEndpoint res : resources) {
					Iterator<AccessPoint> accessPointIterator = res.profile().accessPoints().iterator();
					while (accessPointIterator.hasNext()) {
						ServiceEndpoint.AccessPoint accessPoint = (ServiceEndpoint.AccessPoint) accessPointIterator
								.next();

						if (accessPoint.name().equals("d4science")) {
							keycloakURL = accessPoint.address();
							realm = accessPoint.name();
							clientid = accessPoint.username();
							password = StringEncrypter.getEncrypter().decrypt(accessPoint.password());
							logger.info("Found accesspoint URL = " + keycloakURL);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Unable to retrieve such service endpoint information!", e);
			return;
		} finally {
			if (oldContext != null)
				ScopeProvider.instance.set(oldContext);
		}

		logger.debug("Bean built " + toString());
	}

	/**
	 * Retrieve endpoints information from IS for DB
	 * 
	 * @return list of endpoints for ckan database
	 * @throws Exception
	 */
	private List<ServiceEndpoint> getConfigurationFromIS() throws Exception {

		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Name/text() eq '" + RUNTIME_RESOURCE_NAME + "'");
		query.addCondition("$resource/Profile/Category/text() eq '" + CATEGORY + "'");
		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> toReturn = client.submit(query);
		return toReturn;

	}

	public static KeycloakAPICredentials getSingleton() {
		if (singleton == null)
			singleton = new KeycloakAPICredentials();
		return singleton;
	}

	public String getServerURL() {
		return keycloakURL;
	}

	public String getClientid() {
		return clientid;
	}

	public String getPassword() {
		return password;
	}

	public String getRealm() {
		return realm;
	}

	@Override
	public String toString() {
		return "KeycloakAPICredentials [keycloakURL=" + keycloakURL + ", realm=" + realm + ", clientid=" + clientid
				+ ", password=**************]";
	}

}
