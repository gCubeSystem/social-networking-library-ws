package org.gcube.portal.social.networking.ws.inputs;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.webcohesion.enunciate.metadata.DocumentationExample;

/**
 * Generic input bean for methods that allow to comment posts
 */
@JsonIgnoreProperties(ignoreUnknown = true) // ignore in serialization/deserialization
public class CommentInputBean implements Serializable {

	private static final long serialVersionUID = 5274608088828232980L;

	@JsonProperty("text")
	@NotNull(message = "text cannot be null")
	@Size(min = 1, message = "text cannot be empty")
	@DocumentationExample("I would like to comment that ...")
	/**
	 * text the text of the comment
	 */
	private String text;

	@NotNull(message = "postid cannot be null")
	@JsonProperty("postid")
	/**
	 * postid the postid of the post where you attach the comment
	 */
	private String postid;

	public CommentInputBean() {
		super();
	}

	/**
	 * @param text
	 * @param postid
	 */
	public CommentInputBean(String text, String postid) {
		super();
		this.text = text;
		this.postid = postid;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPostid() {
		return postid;
	}

	public void setPostid(String postid) {
		this.postid = postid;
	}

	@Override
	public String toString() {
		return "CommentInputBean [text=" + text + ", postid=" + postid + "]";
	}

}
