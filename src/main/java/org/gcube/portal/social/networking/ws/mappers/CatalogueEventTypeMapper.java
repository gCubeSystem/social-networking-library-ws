package org.gcube.portal.social.networking.ws.mappers;

import org.gcube.portal.databook.shared.NotificationType;
import org.gcube.portal.databook.shared.ex.NotificationTypeNotFoundException;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;

public class CatalogueEventTypeMapper {

	public CatalogueEventTypeMapper() {
	}

	public static NotificationType getType(CatalogueEventType type) throws NotificationTypeNotFoundException {
		switch (type) {
			case ITEM_PUBLISHED:
				return NotificationType.CAT_ITEM_PUBLISHED;
			case ITEM_REJECTED:
				return NotificationType.CAT_ITEM_REJECTED;
			case ITEM_REMOVED:
				return NotificationType.CAT_ITEM_DELETE;
			case ITEM_SUBMITTED:
				return NotificationType.CAT_ITEM_SUBMITTED;
			case ITEM_UPDATED:
				return NotificationType.CAT_ITEM_UPDATED;
			default:
				throw new NotificationTypeNotFoundException("The Catalogue event could not be mapped");
		}
	}
}
