package org.gcube.portal.social.networking.ws.inputs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 *
 */
public class UserSetNotificationBean {

	@JsonProperty("username")
	@NotNull(message = "username cannot be null")
	@Size(message = "username cannot be empty", min = 1)
	private String username;

	@JsonProperty("disableNotification")
	@NotNull(message = "disableNotification cannot be null")
	private Boolean disableNotification;

	public UserSetNotificationBean() {
		super();
	}

	/**
	 * @param username
	 * @param disableNotification
	 */
	public UserSetNotificationBean(String username, boolean disableNotification) {
		super();
		this.username = username;
		this.disableNotification = disableNotification;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isDisableNotification() {
		return disableNotification;
	}

	public void setDisableNotification(boolean disableNotification) {
		this.disableNotification = disableNotification;
	}

	@Override
	public String toString() {
		return "UserSetNotificationBean [username=" + username + ", disableNotification=" + disableNotification + "]";
	}

}
